/* Reference: http://alpha.jewelbots.com/t/working-with-leds/66 */

/*
 * Note: Using the function described in the reference doc
 * with my Jewelbot doesn't work - it just displays
 * a white light, and it doesn't delay at all.
 * 
 * I've written another version "colors_and_buzzing_Direct"
 * that actually works, using the functions I've found
 * from the API's inner-workings (yay, open source)
 */

LED led;

void setup() 
{
}

void loop() 
{
  //setLed( 0, 1 );
  led.on( 0, "red", 1000000 );

  //setLed( 1, 2 );
  led.on( 1, "green", 1000000 );
  
  //setLed( 2, 3 );
  led.on( 2, "blue", 1000000 );
  
  //setLed( 3, 4 );
  led.on( 3, "yellow", 1000000 );
}





