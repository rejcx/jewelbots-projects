
#include "LED.h"
#include "led_sequence.h"
#include "app_error.h"
#include "app_timer.h"
#include "common_defines.h"
#include "haptics_driver.h"
#include "jewelbot_gpio.h"
#include "led_driver.h"
#include "nordic_common.h"
#include "nrf.h"
#include "nrf_delay.h"
#include "nrf_gpio.h"
#include "nrf_log.h"
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "Arduino.h"


extern "C"{




  LED::LED()
  {
    enable_led();
    clear_led();
  }

  LED::~LED()
  {}
  
  void LED::turnOn(LED_Pos led, Color color)
  {
      setLight(uint8_t(led), color.r, color.g, color.b);
  }

  void LED::turnOff(LED_Pos led)
  {
      setLight(uint8_t(led), COLORS[ 0 ].r, COLORS[ 0 ].g, COLORS[ 0 ].b);
  }
  
  void LED::setLight(uint8_t number, uint8_t r, uint8_t g, uint8_t b)
  {
    led_cmd_t options[4] = {number, r, g, b, 1};
    set_led_state_handler(options);
  }



} // extern "C"
