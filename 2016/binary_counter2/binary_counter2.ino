/*
Counts from 0 to 60 in binary, using each LED as one binary digit.

It counts from 1 to 10 6 times, with a different color for each set.
*/

#include "LED.h"
#include "LED_Colors.h"

/*
 * 0 = 0000     8 = 1000
 * 1 = 0001     9 = 1001
 * 2 = 0010     10 = 1010
 * 3 = 0011     11 = 1011
 * 4 = 0100     12 = 1100
 * 5 = 0101     13 = 1101
 * 6 = 0110     14 = 1110
 * 7 = 0111     15 = 1111
 */


Color colors[6] = {
  Color( 0x10, 0x00, 0x00 ),    // Red
  Color( 0x00, 0x10, 0x00 ),    // Green
  Color( 0x00, 0x00, 0x10 ),    // Blue
  Color( 0x10, 0x10, 0x00 ),    // Yellow
  Color( 0x10, 0x00, 0x10 ),    // Magenta
  Color( 0x00, 0x10, 0x10 )     // Cyan
};

Color off;
int onesCounter;
int tensCounter;
LED led;

void setup() {
  onesCounter = 0;
  tensCounter = 0;

  off.r = 0x00;
  off.g = 0x00;
  off.b = 0x00;
  
  enable_led();
  clear_led();
}

void loop() {
  onesCounter++;

  for ( int i = 0; i < 4; i++ )
  {
    turnOff( i );
  }

  if ( onesCounter % 2 == 1 ) { turnOn( 0, colors[ tensCounter ] ); }
  if ( onesCounter % 4 >= 2 ) { turnOn( 1, colors[ tensCounter ] ); }
  if ( onesCounter % 8 >= 4 ) { turnOn( 2, colors[ tensCounter ] ); }
  if ( onesCounter % 16 >= 8 ) { turnOn( 3, colors[ tensCounter ] ); }

  if ( onesCounter >= 10 )
  {
    onesCounter = 0;
    tensCounter += 1;
  }

  if ( tensCounter >= 6 )
  {
    tensCounter = 0;
  }

  waitASecond();
}



void waitASecond()
{
  nrf_delay_us( 1000000 );
}
void turnOn(int index, Color color)
{
    led_cmd_t options[4] = {index, color.r, color.g, color.b, 1};
    set_led_state_handler(options);
}

void turnOff(int index)
{
    led_cmd_t options[4] = {index, off.r, off.g, off.b, 1};
    set_led_state_handler(options);
}
