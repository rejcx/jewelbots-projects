/* Reference: https://github.com/Jewelbots/arduino-library/tree/master/jewelbots_nRF51822_board_v100/cores/JWB_nRF51822/ble-nrf51822-master/source */

#include "led_driver.h"
#include "haptics_driver.h"

struct Color
{
  Color( uint8_t r, uint8_t g, uint8_t b )
  {
    this->r = r;
    this->g = g;
    this->b = b;
  }
  
  uint8_t r, g, b;
};

Color colors[8] = {
  Color( 0x00, 0x00, 0x00 ), // Off
  Color( 0x3F, 0x00, 0x00 ), // Red
  Color( 0x00, 0x3F, 0x00 ), // Green
  Color( 0x00, 0x00, 0x3F ), // Blue
  Color( 0x3F, 0x3F, 0x00 ), // Yellow
  Color( 0x3F, 0x00, 0x3F ), // Magenta
  Color( 0x00, 0x3F, 0x3F ), // Cyan
  Color( 0x3F, 0x3F, 0x3F ) // White
};

void setup() 
{
  haptics_init();
  haptics_enable();
  
  enable_led();
  clear_led();
}

// led_driver.c
void setLed( int index, int color )
{
  led_cmd_t options[4] = { index, 
    colors[ color ].r, 
    colors[ color ].g,
    colors[ color ].b,
    1 };
  set_led_state_handler( options );  
}

// haptics_driver.c
void shortBuzz() 
{
  haptics_msg_short();
}

void longBuzz() 
{
  haptics_msg_long();
}

// nrf_delay in nordic_sdk/components/drivers_nrf
void waitASecond()
{
  nrf_delay_us( 1000000 );
}

void loop() 
{
  setLed( 0, 1 );
  shortBuzz();
  waitASecond();

  
  setLed( 1, 2 );
  shortBuzz();
  waitASecond();

  
  setLed( 2, 3 );
  shortBuzz();
  waitASecond();

  
  setLed( 3, 4 );
  shortBuzz();
  waitASecond();

  longBuzz();
  
  setLed( 0, 0 );
  setLed( 1, 0 );
  setLed( 2, 0 );
  setLed( 3, 0 );
  
  waitASecond();
}




