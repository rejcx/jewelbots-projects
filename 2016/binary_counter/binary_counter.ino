/*
Counts from 0 to 15 in binary, using each LED as one binary digit.
See chart below.
*/

#include "LED.h"
#include "LED_Colors.h"

/*
 * 0 = 0000     8 = 1000
 * 1 = 0001     9 = 1001
 * 2 = 0010     10 = 1010
 * 3 = 0011     11 = 1011
 * 4 = 0100     12 = 1100
 * 5 = 0101     13 = 1101
 * 6 = 0110     14 = 1110
 * 7 = 0111     15 = 1111
 */

Color mainColor;
Color off;
int counter;
LED led;

void setup() {
  counter = 0;
  
  mainColor.r = 0x10;
  mainColor.g = 0x00;
  mainColor.b = 0x00;

  off.r = 0x00;
  off.g = 0x00;
  off.b = 0x00;
  
  enable_led();
  clear_led();
}

void loop() {
  counter++;

  for ( int i = 0; i < 4; i++ )
  {
    turnOff( i );
  }

  if ( counter % 2 == 1 ) { turnOn( 0, mainColor ); }
  if ( counter % 4 >= 2 ) { turnOn( 1, mainColor ); }
  if ( counter % 8 >= 4 ) { turnOn( 2, mainColor ); }
  if ( counter % 16 >= 8 ) { turnOn( 3, mainColor ); }

  if ( counter == 16 )
  {
    counter = 0;
  }

  waitASecond();
}



void waitASecond()
{
  nrf_delay_us( 1000000 );
}
void turnOn(int index, Color color)
{
    led_cmd_t options[4] = {index, color.r, color.g, color.b, 1};
    set_led_state_handler(options);
}

void turnOff(int index)
{
    led_cmd_t options[4] = {index, off.r, off.g, off.b, 1};
    set_led_state_handler(options);
}
