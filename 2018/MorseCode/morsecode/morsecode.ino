// Morse Code app - Rachel Singh (MoosaderRach)

LED led;

void setup() {
}
// Dots are 1 time unit,
// Dashes are 3 time units,
// Space between symbols is 1 time unit,
// Space between letters is 3 time units,
// Space between words is 7 time units
  
void loop() {
  MorseCodeFlash( 'a' );
  MorseCodeFlash( 'b' );
  MorseCodeFlash( 'c' );
  MorseCodeFlash( 'd' );
  MorseCodeFlash( 'e' );
  MorseCodeFlash( 'f' );
  MorseCodeFlash( 'g' );
  MorseCodeFlash( 'h' );
  MorseCodeFlash( 'i' );
  MorseCodeFlash( 'j' );
  MorseCodeFlash( 'l' );
  MorseCodeFlash( 'm' );
  MorseCodeFlash( 'n' );
  MorseCodeFlash( 'o' );
  MorseCodeFlash( 'p' );
  MorseCodeFlash( 'q' );
  MorseCodeFlash( 'r' );
  MorseCodeFlash( 's' );
  MorseCodeFlash( 't' );
  MorseCodeFlash( 'u' );
  MorseCodeFlash( 'v' );
  MorseCodeFlash( 'w' );
  MorseCodeFlash( 'x' );
  MorseCodeFlash( 'y' );
  MorseCodeFlash( 'z' );
  EndWord();
}

void ShowDit()
{
    Clear();
    led.turn_on_single(NW, GREEN);
    WaitSeconds( 1 );
    Clear();
    WaitSeconds( 1 );
}

void ShowDah()
{
    Clear();
    led.turn_on_single(SW, GREEN);
    led.turn_on_single(NW, GREEN); 
    WaitSeconds( 3 );
    Clear();
    WaitSeconds( 1 ); 
}

void Clear()
{
  led.turn_off_all();
}

void EndLetter()
{
  Clear();
  WaitSeconds( 3 );
}

void EndWord()
{
  Clear();
  WaitSeconds( 7 );
}

void WaitSeconds( int seconds )
{
  nrf_delay_us( seconds * 1000000 );
}

void MorseCodeFlash( char letter )
{
  switch( letter )
  {
    case 'a':   // .-
    ShowDit();
    ShowDah();
    EndLetter();
    break;

    case 'b': // -...
    ShowDah();
    ShowDit();
    ShowDit();
    ShowDit();
    EndLetter();
    break;

    case 'c': // -.-.
    ShowDah();
    ShowDit();
    ShowDah();
    ShowDit();
    EndLetter();
    break;

    case 'd': // -..
    ShowDah();
    ShowDit();
    ShowDit();
    EndLetter();
    break;

    case 'e': // .
    ShowDit();
    EndLetter();
    break;

    case 'f': // ..-.
    ShowDit();
    ShowDit();
    ShowDah();
    ShowDit();
    EndLetter();
    break;

    case 'g': // --.
    ShowDah();
    ShowDah();
    ShowDit();
    EndLetter();
    break;

    case 'h': // ...
    ShowDit();
    ShowDit();
    ShowDit();
    ShowDit();
    EndLetter();
    break;

    case 'i': // ..
    ShowDit();
    ShowDit();
    EndLetter();
    break;

    case 'j': // .---
    ShowDit();
    ShowDah();
    ShowDah();
    ShowDah();
    EndLetter();
    break;
    
    case 'k': // -.-
    ShowDah();
    ShowDit();
    ShowDah();
    EndLetter();
    break;

    case 'l': // .-..
    ShowDit();
    ShowDah();
    ShowDit();
    ShowDit();
    EndLetter();
    break;

    case 'm': // --
    ShowDah();
    ShowDah();
    EndLetter();
    break;

    case 'n': // -.
    ShowDah();
    ShowDit();
    EndLetter();
    break;

    case 'o': // ---
    ShowDah();
    ShowDah();
    ShowDah();
    EndLetter();
    break;

    case 'p': // .--.
    ShowDit();
    ShowDah();
    ShowDah();
    ShowDit();
    EndLetter();
    break;

    case 'q': // --.-
    ShowDah();
    ShowDah();
    ShowDit();
    ShowDah();
    EndLetter();
    break;

    case 'r': // .-.
    ShowDit();
    ShowDah();
    ShowDit();
    EndLetter();
    break;

    case 's': // ...
    ShowDit();
    ShowDit();
    ShowDit();
    EndLetter();
    break;

    case 't': // -
    ShowDah();
    EndLetter();
    break;

    case 'u': // ..-
    ShowDit();
    ShowDit();
    ShowDah();
    EndLetter();
    break;

    case 'v': // ...-
    ShowDit();
    ShowDit();
    ShowDit();
    ShowDah();
    EndLetter();
    break;

    case 'w': // .--
    ShowDit();
    ShowDah();
    ShowDah();
    EndLetter();    
    break;

    case 'y': // -.--
    ShowDah();
    ShowDit();
    ShowDah();
    ShowDah();
    EndLetter();
    break;

    case 'z': // --..
    ShowDah();
    ShowDah();
    ShowDit();
    ShowDit();
    EndLetter();
    break;

    case ' ':
    EndLetter();
    break;
  }
}

